# ini and yaml Editor Obsidian Plugin
A plugin for [Obsidian](https://obsidian.md) which allows editing of .ini files.

### Compatibility

The current API of this repo targets Obsidian **v0.10.11**.  
It won't work in versions below that due to the apis used only being exposed in 0.10.11

### Notes
This is all very much just a proof of concept for now. Expect bugs and data loss. This is just to prove to myself I *can* handle custom files, before moving on to more interesting files (e.g. CSV) in future.

## Development

This is a homework project in progress, using as a base a plugin made by deathau https://github.com/deathau/ini-obsidian


# Version History
## 0.0.1
Initial release!
- You can open and edit ini files
- buggy (for some reason the text doesn't appear until you click on it the first time)

## 0.0.2
Homework
- you can open and edit yaml files
