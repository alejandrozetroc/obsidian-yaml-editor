import typescript from '@rollup/plugin-typescript';
import {nodeResolve} from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import scss from 'rollup-plugin-scss';
import { readFileSync } from 'fs';
import copy from 'rollup-plugin-copy';

const packageJson = JSON.parse(readFileSync('package.json'))
const TEST_VAULT = `test-vault/.obsidian/plugins/${packageJson.name}`;

export default {
  input: 'src/main.ts',
  output: [
      {
      dir: '.',
      sourcemap: 'inline',
      format: 'cjs',
      exports: 'default'
    },
    {
      dir: TEST_VAULT,
      sourcemap: 'inline',
      format: 'cjs',
      exports: 'default'
    }
  ],
  external: ['obsidian'],
  plugins: [
    typescript(),
    nodeResolve({browser: true}),
    commonjs(),
    scss({ output: 'styles.css', sass: require('sass') }),
    copy({
      flatten: true,
      targets: [
        { src: ['manifest.json', 'styles.css'], dest: TEST_VAULT }
      ]
    })
  ]
};