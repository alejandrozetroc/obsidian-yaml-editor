import './styles.scss'
import { Plugin, WorkspaceLeaf, addIcon, TextFileView } from 'obsidian';
import './lib/codemirror'
import './mode/properties/properties'

const icon = (extension: string) => `
  <path fill="currentColor" stroke="currentColor" d="M14,4v92h72V29.2l-0.6-0.6l-24-24L60.8,4L14,4z M18,8h40v24h24v60H18L18,8z M62,10.9L79.1,28H62V10.9z"></path>
  <text font-family="sans-serif" font-weight="bold" font-size="30" fill="currentColor" x="50%" y="60%" dominant-baseline="middle" text-anchor="middle">
    ${extension}
  </text>
`;

const SUPPORT_FILES = [
  'ini',
  'yaml',
]

const VIEW_NAME = 'simple-file-editor'

export default class IniPlugin extends Plugin {
  // settings: any;
  static fileType: string = 'ini';

  async onload() {
    super.onload();
    // this.settings = await this.loadData() || {} as any;
    
    this.registerView(VIEW_NAME, this.iniViewCreator);
    
    for (const file of SUPPORT_FILES) {
      addIcon(`document-${file}`, icon(file))
    }

    this.registerExtensions(SUPPORT_FILES, VIEW_NAME);
  }

  // function to create the view
  iniViewCreator = (leaf: WorkspaceLeaf) => {
    return new IniView(leaf);
  }
}

// This is the custom view
class IniView extends TextFileView {

  // internal code mirror instance
  codeMirror: CodeMirror.Editor;

  // this.contentEl is not exposed, so cheat a bit.
  public get extContentEl(): HTMLElement {
    // @ts-ignore
    return this.contentEl;
  }

  // constructor
  constructor(leaf: WorkspaceLeaf) {
    super(leaf);

    // create code mirror instance
    this.codeMirror = CodeMirror(this.extContentEl, {
      theme: "obsidian"
    });
    // register the changes event
    this.codeMirror.on('changes', this.changed);

    app.workspace.on('active-leaf-change', (e: WorkspaceLeaf | null) => {
      const { extension } = app.workspace.getActiveFile();
      if (this.fileContents) {
        this.codeMirror.swapDoc(CodeMirror.Doc(this.fileContents, `text/x-${extension}`))
      }
    })
  }

  // when the view is resized, refresh CodeMirror (thanks Licat!)
  onResize() {
    this.codeMirror.refresh();
  }

  // called on code mirror changes
  changed = async (instance: CodeMirror.Editor, changes: CodeMirror.EditorChangeLinkedList[]) => {
    // request a debounced save in 2 seconds from now
    this.requestSave();
  }

  // get the new file contents
  getViewData = () => {
    return this.codeMirror.getValue();
  }

  fileContents: string

  // set the file contents
  setViewData = (data: string, clear: boolean) => {
    this.fileContents = data;
    if (clear) {
      this.codeMirror.swapDoc(CodeMirror.Doc(data, `text/x-txt`))
    }
    else {
      this.codeMirror.setValue(data);
    }
  }

  // clear the view content
  clear = () => {
    this.codeMirror.setValue('');
    this.codeMirror.clearHistory();
  }

  // gets the title of the document
  getDisplayText() {
    if (this.file) return this.file.basename;
    else return "ini (no file)";
  }

  // the view type name
  getViewType() {
    return VIEW_NAME;
  }

  getFileType(): string {
    throw new Error('Method not implemented.');
  }

  // icon for the view
  getIcon() {
    return `document-${VIEW_NAME}`;
  }
}